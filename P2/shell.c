#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pwd.h>
 
int command_pipe[2];
static char* args[512];
pid_t pid;

 
#define READ  0
#define WRITE 1
const char *getUserName()
{
  uid_t uid = geteuid();
  struct passwd *pw = getpwuid(uid);
  if (pw)
  {
    return pw->pw_name;
  }

  return "";
}

static int command(int input, int first, int last)
{
	int pipettes[2];
 
	//for piping
	pipe( pipettes );	
	pid = fork();
 
 
	if (pid == 0) {
		if (first == 1 && last == 0 && input == 0) {
			// First
			dup2( pipettes[WRITE], STDOUT_FILENO );
		} else if (first == 0 && last == 0 && input != 0) {
			// Middle
			dup2(input, STDIN_FILENO);
			dup2(pipettes[WRITE], STDOUT_FILENO);
		} else {
			// Last
			dup2( input, STDIN_FILENO );
		}
 
		if (execvp( args[0], args) == -1)
			_exit(EXIT_FAILURE); // If child fails
	}
 
	if (input != 0) 
		close(input);
 
	close(pipettes[WRITE]);// it is finished and nothing more needs to be writen
 

	if (last == 1)//it is the last command so nothing more needs to be read
		close(pipettes[READ]);
 
	return pipettes[READ];
}
 
static void cleanup(int n) // n: number of command was invoked
{
	int i;
	for (i = 0; i < n; ++i) 
		wait(NULL); 
}
 
static int run(char* cmd, int input, int first, int last);
static char line[1024];
static int n = 0; /* number of calls to 'command' */
 
int main()
{
	printf("Try Our Shell: Type 'exit' or send EOF to exit.\n");
	while (1) {

		char* userName = getUserName();
		printf("%s", userName);
		printf("$ ");
		fflush(NULL);
		
		
		int input = 0;
		int first = 1; 
		char* cmd = line;
		//for finding first |
		char* next = strchr(cmd, '|');

 
		//let's read a new line
		if (!fgets(line, 1024, stdin)) 
			return 0;
  
		//'next' points to '|'
		while (next != NULL) {
			*next = '\0';
			input = run(cmd, input, first, 0);
			first = 0;
			cmd = next + 1;
			next = strchr(cmd, '|');
		}
		input = run(cmd, input, first, 1);
		cleanup(n);
		n = 0;
	}
	return 0;
}
 
static void split(char* cmd);
 
static int run(char* cmd, int input, int first, int last)
{
	split(cmd);
	if (args[0] != NULL) {
		if (strcmp(args[0], "exit") == 0) 
			exit(0);
		n += 1;
		return command(input, first, last);
	}
	return 0;
}
 
static char* skipwhite(char* s)
{
	while (isspace(*s)) ++s;
	return s;
}
 
static void split(char* cmd)
{
	cmd = skipwhite(cmd);
	char* next = strchr(cmd, ' ');
	int i = 0;
 
	while(next != NULL) {
		next[0] = '\0';
		args[i] = cmd;
		i++;
		cmd = skipwhite(next + 1);
		next = strchr(cmd, ' ');
	}
 
	if (cmd[0] != '\0') {
		args[i] = cmd;
		next = strchr(cmd, '\n');
		next[0] = '\0';
		i++; 
	}
 
	args[i] = NULL;
}
