#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

int GetCPULoad(){
int FileHandler;
char FileBuffer[1024];
float load;

FileHandler = open("/proc/loadavg", O_RDONLY);
if(FileHandler < 0){
return -1;}
read(FileHandler, FileBuffer, sizeof(FileBuffer) - 1);
sscanf(FileBuffer, "%f", &load);
close(FileHandler);
return (int)(load * 100);
}

int main(void){
printf("%d\n", GetCPULoad());
return 0;
}
